<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class projects_model extends CI_Model{

	public function getProj(){
		$this->db->select("*");
		$this->db->from("projects");
		$where = "status_project != 'DONE' AND status_project !='EXPIRED'";
		$this->db->where($where);
		//$this->db->or_where('status_project != "EXPIRED"');
		$result=$this->db->get();
		return $result->result_array();
	}

	public function getProj2(){
		$result = $this->db->query("SELECT * from projects Order by due_date ASC Limit 5");
		return $result->result_array();
	}

	public function getProjwithEmail($email){
		$this->db->select("*");
		$this->db->from("projects");
		$where = "email_user='$email'";
		$this->db->where($where);
		$result=$this->db->get();
		return $result->result_array();
	}

	public function getAllCollabs($kode_project){
		$this->db->select("*");
		
		$result=$this->db->get_where('kaloborators_transaction', array('kode_project' => $kode_project));
		//$this->db->query('SELECT * FROM kaloborators_transaction where kode_project='.$kode_project);
		//$result = $this->db->get();
		return $result->result_array();
	}


	public function addProj($email_user, $nama_project, $project_desc, $target, $no_kontak, $required_person,$due_date){
		$query = $this->db->query('SELECT * FROM projects');
		$counts = $query->num_rows() +1;
		$data = array(
        'kode_project' => "proj0".$counts,
        'email_user' => $email_user,
        'nama_project' => $nama_project,
        'project_desc' => $project_desc,
        'target'=> $target,
        'no_kontak' => $no_kontak,
        'status_project' => "ONGOING",
        'required_person' => $required_person,
        'due_date' => $due_date
		);

		return $this->db->insert('projects', $data);
	}

	public function addCollab($kode_project, $email_user){
	
		$data = array(
        'kode_project' => $kode_project,
        'email_user' => $email_user
		);
		return $this->db->insert('kaloborators_transaction', $data);
	}

	public function checkCollab($email_user,$kode_project){
		$query = $this->db->get_where('kaloborators_transaction', array('email_user' => $email_user,'kode_project'=>$kode_project));
		return $query->row();
	}

	public function updateProject(){
		$this->db->set('status_project', 'EXPIRED');
		$where="due_date < CURDATE()";
		$this->db->where($where);
		$this->db->update('projects');
	}

	public function endProject($kode){
		$this->db->set('status_project', 'DONE');
		$where="kode_project='".$kode."'";
		$this->db->where($where);
		$this->db->update('projects');
	}
}

 ?>


