<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class product_model extends CI_Model{


	public function getProducts(){
		$query = $this->db->query("SELECT * from item_sell Order by upload_date ASC Limit 4");

		return $query->result_array();
  }

  public function getProducts2(){
		$query = $this->db->query("SELECT * from item_sell");

		return $query->result_array();
  }

  	public function add($nama_item, $berat, $itemsell_desc, $harga, $picture_link){
		$query = $this->db->query('SELECT * FROM item_sell');
		$counts = $query->num_rows() +1;
		$data = array(
		'kode_item' => "item0".$counts,
		'nama_item' => $nama_item,
		'berat' => $berat,
		'itemsell_desc' => $itemsell_desc,
		'harga' => $harga,
		'jumlah_terjual' => 0,
		'picture_link' => $picture_link
		);

		return $this->db->insert('item_sell',$data);
	}

	public function getMyOrder($email){
		$query = $this->db->query("SELECT * from transaction_item Where email_user='".$email."'");

		return $query->result_array();
	}

	public function addTransaction($nama_penerima, $alamat_penerima, $kurir, $kode_item, $qty, $harga, $email){
		$query = $this->db->query('SELECT * FROM transaction_item');
		$counts = $query->num_rows() +1;
		$data = array(
		'kode_transaksi' => "trans0".$counts,
		'email_user' => $email,
		'kode_item' => $kode_item,
		'qty' => $qty,
		'kurir' => $kurir,
		'total' => $harga * $qty,
		'nama_penerima' => $nama_penerima,
		'alamat_penerima' => $alamat_penerima,
		'transaction_date' => date('Y-m-d'),
		'status' => "WAITING FOR PAYMENT"
		);

		$this->db->insert('transaction_item',$data);
		return "trans0".$counts;
	}

	public function getImageName(){
		$query = $this->db->query('SELECT * FROM payment');
		$counts = $query->num_rows() +1;
		return "bkt".$counts.".";
	}

	public function addBukti($email, $kode_transaksi, $nama_pemilik, $no_rek, $bank, $nama_image){
		$data = array(
		'email_user' => $email,
		'kode_transaksi'=> $kode_transaksi,
		'nama_pemilik' => $nama_pemilik,
		'no_rek' => $no_rek,
		'bank' => $bank,
		'nama_image' => $nama_image
		);

		$this->db->insert('payment',$data);
	}

	public function getTransaction(){
		$this->db->select("*");
		$this->db->from("transaction_item");
		$result=$this->db->get();
		return $result->result_array();
	}

	public function updateTransaction($kode_trans, $status){
		$this->db->set('status', $status);
		$where="kode_transaksi='".$kode_trans."'";
		$this->db->where($where);
		$this->db->update('transaction_item');
	}
	}

?>