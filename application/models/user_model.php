<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class user_model extends CI_Model{

	public function login($email, $password){
		$query = $this->db->get_where('user', array('email' => $email, 'password'=> md5($password)));
		return $query->row();
	}

	public function checkData($email){
		$query = $this->db->get_where('user', array('email' => $email));
		return $query->row();
	}

	public function getEmps(){
		$this->db->select("*");
		$this->db->from("user");
		$result = $this->db->get();
		return $result->result_array();
	}

	public function addEmp($email, $password, $fullname, $tanggal_lahir, $kode_pos){
		$data = array(
        'email' => $email,
        'nama' => $fullname,
        'password' => md5($password),
        'tgl_lahir' => $tanggal_lahir,
        'kode_pos' => $kode_pos
		);

		return $this->db->insert('user', $data);
	}


	public function deleteEmp($userid){
		return $this->db->delete('users', array('user_id' => $userid));
	}

	public function minPoin($email,$poin){
		$this->db->set('gift_point', $poin);
		$where="email='".$email."'";
		$this->db->where($where);
		$this->db->update('user');
	}
	
}
?>