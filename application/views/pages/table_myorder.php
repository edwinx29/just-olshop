<div class="container">
		
		<br>
		</div>
		<table id="projects" cellspacing="0" width="100%" class="display table table-striped table-bordered">
			<thead>
				<th>No</th>
				<th>Kode Order</th>
				<th>Nama Barang</th>
				<th>Harga</th>
				<th>Qty</th>
				<th>Total</th>
				<th>Kurir</th>
				<th>Status Pesanan</th>
				<th></th>
			</thead>
			
			<tbody>
			<?php foreach($arr as $key=>$value): ?>
				<tr>
						<td><?php echo $key + 1 ?></td>
						<td><?php echo $value['kode_transaksi']; ?></td>
						<td><?php echo $value['kode_item']; ?></td>
						<td><?php echo $value['transaction_date']; ?></td>
						<td><?php echo $value['qty']; ?></td>
						<td><?php echo $value['total']; ?></td>
						<td><?php echo $value['kurir']; ?></td>
						<td><?php echo $value['status']; ?></td>
						<td><?php if($value['status']=="WAITING FOR PAYMENT"){?>
							<form action='<?php echo base_url()?>index.php/CheckoutController/upload_view' method='POST'>
							<input type='hidden' name='kode_transaksi' value="<?php echo $value['kode_transaksi']; ?>">
							<button class='btn btn-info btn-block' name='btnbukti' type='submit' >
        							 Kirim Bukti Transfer
        						</button>
							</form>
						<?php }?>
						</td>
						
				</tr>
			<?php endforeach; ?>
			</tbody>
		</table>

		<script type="text/javascript">
			$(document).ready(function() {
			    $('#projects').DataTable();
			} );
		</script>
</div>