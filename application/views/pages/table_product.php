<div class="container">
		
		<br>
		</div>
		<table id="projects" cellspacing="0" width="100%" class="display table table-striped table-bordered">
			<thead>
				<th>No</th>
				<th>Kode Item</th>
				<th>Nama Item</th>
				<th>Item Description</th>
				<th>Harga</th>
				<th width="20%">Picture</th>
				<th>Tanggal Upload</th>
			</thead>
			
			<tbody>
			<?php foreach($arr as $key=>$value): ?>
				<tr>
						<td><?php echo $key + 1 ?></td>
						<td><?php echo $value['kode_item']; ?></td>
						<td><?php echo $value['nama_item']; ?></td>
						<td><?php echo $value['itemsell_desc']?></td>
						<td><?php echo $value['harga']; ?></td>
						<td><img src="<?php echo $value['picture_link']; ?>" height="42" width="42"></td>
						<td><?php echo $value['upload_date']; ?></td>
						<form action='<?php echo base_url()?>index.php/AddItemController/action' method='POST'>
							<input type='hidden' name='kode_item' value="<?php echo $value['kode_item']; ?>">
							<input type='hidden' name='nama_item' value="<?php echo $value['nama_item']; ?>">
							<input type='hidden' name='berat' value="<?php echo $value['berat']; ?>">
							<input type='hidden' name='itemsell_desc' value="<?php echo $value['itemsell_desc']; ?>">
							<input type='hidden' name='harga' value="<?php echo $value['harga']; ?>">
							<input type='hidden' name='jumlah_terjual' value="<?php echo $value['jumlah_terjual']; ?>">
							<input type='hidden' name='picture_link' value="<?php echo $value['picture_link']; ?>">
							<input type='hidden' name='upload_date' value="<?php echo $value['upload_date']; ?>">
        				
				</tr>
			<?php endforeach; ?>
			</tbody>
		</table>

		<script type="text/javascript">
			$(document).ready(function() {
			    $('#projects').DataTable();
			} );
		</script>
</div>