<div class="container">
		
		<br>
		</div>
		<form action='<?php echo base_url()?>index.php/LaporanTransaksiController/update' method='POST'>
		<table id="projects" cellspacing="0" width="100%" class="display table table-striped table-bordered">
			<thead>
				<th>No</th>
				<th>Kode Transaksi</th>
				<th>Email Pelanggan</th>
				<th>Kode Item</th>
				<th>Jumlah</th>
				<th>Kurir</th>
				<th>Total</th>
				<th>Nama Penerima</th>
				<th>Alamat Penerima</th>
				<th>Tanggal Transaksi</th>
				<th>Status</th>
			</thead>
			
			<tbody>
			<?php foreach($arr as $key=>$value): ?>
				<tr>
						<td><?php echo $key + 1 ?></td>
						<td><?php echo $value['kode_transaksi']; ?></td>
						<td><?php echo $value['email_user']; ?></td>
						<td><?php echo $value['kode_item']; ?></td>
						<td><?php echo $value['qty']?></td>
						<td><?php echo $value['kurir']; ?></td>
						<td><?php echo $value['total']; ?></td>
						<td><?php echo $value['nama_penerima']; ?></td>
						<td><?php echo $value['alamat_penerima']; ?></td>
						<td><?php echo $value['transaction_date']; ?></td>
						<td><select class="srchTxt" name="status">
								<option value="WAITING FOR PAYMENT" <?php if($value['status']=="WAITING FOR PAYMENT") echo 'selected="selected"';?>>WAITING FOR PAYMENT</option>
								<option value="WAITING FOR CONFIRMATION" <?php if($value['status']=="WAITING FOR CONFIRMATION") echo 'selected="selected"';?>>WAITING FOR CONFIRMATION</option>
								<option value="SENDING" <?php if($value['status']=="SENDING") echo 'selected="selected"';?>>SENDING</option>
								<option value="SENT" <?php if($value['status']=="SENT") echo 'selected="selected"';?>>SENT</option>
							</select> 
							</td>
						<td>
							<input type='hidden' name='kode_transaksi' value="<?php echo $value['kode_transaksi']; ?>">
							<input type='hidden' name='email_user' value="<?php echo $value['email_user']; ?>">
							<input type='hidden' name='kode_item' value="<?php echo $value['kode_item']; ?>">
							<input type='hidden' name='qty' value="<?php echo $value['qty']; ?>">
							<input type='hidden' name='kurir' value="<?php echo $value['kurir']; ?>">
							<input type='hidden' name='nama_penerima' value="<?php echo $value['nama_penerima']; ?>">
							<input type='hidden' name='alamat_penerima' value="<?php echo $value['alamat_penerima']; ?>">
							<input type='hidden' name='transaction_date' value="<?php echo $value['transaction_date']; ?>">
        						<button class='btn btn-info btn-block' name='btnUpdate' type='submit' <?php if($value['status']=="SENT" || $value['status']=="WAITING FOR PAYMENT") echo 'disabled';?> >
        							 Update
        						</button>
        					
        				</td>
				</tr>
			<?php endforeach; ?>
			</tbody>
		</table>
		</form>
		<script type="text/javascript">
			$(document).ready(function() {
			    $('#projects').DataTable();
			} );
		</script>
</div>