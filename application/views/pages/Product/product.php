<!DOCTYPE html>
<html>
<head>
	<title>Product of Gift Code</title>
</head>
<body>
	 <!-- Bootstrap style --> 
<!-- <table>
	<tr>
		<th>kode merchant</th>
		<th>kode item</th>
		<th>nama_item</th>
		<th>berat</th>
		<th>itemsell_desc</th>
		<th>harga</th>
		<th>jumlah_terjual</th>
	</tr> -->

	<?php 
	echo $js;
	echo $css;
	echo $header;
	 ?>

<div id="mainBody">
	<div class="container">
			<?php if(isset($_SESSION['email'])){ ?>
				<form class="form-horizontal" id="item_form" action="<?php echo base_url()?>index.php/ProductController/myOrder" method="post">
				<div class="form-group col-xs-7" style="float:right;">
						<input type="submit" class="btn btn-large btn-primary" name="submit" value="My Order" ></input>
				</div>
				</form>
			<?php }?>

			<div class="container">
				<h4>All Product </h4>
				
		<ul class="thumbnails">
	<?php 
	foreach($product as $row){

		$kode_item = $row['kode_item'];
		$nama_item = $row['nama_item'];
		$berat = $row['berat'];
		$itemsell_desc = $row['itemsell_desc'];
		$harga = $row['harga'];
		$jumlah_terjual = $row['jumlah_terjual'];
		$picture_link = $row['picture_link'];
		?>

			  
				<li class="span3" >
				  <div class="thumbnail">
				  <i class="tag"></i>
				  	<a href="<?php echo base_url()?>index.php/BuyProductController"><?php echo '<img src="'.$picture_link.'" alt="Cover" style="height:200px;width:1000px">'; ?></a>
					<div class="caption">
					  <!-- <h4><a class="btn" href="#">VIEW</a> -->
					   <h5><?php echo $nama_item ;?></h5>
					        <span class="pull-right"><?php echo "Rp.".$harga ;?></span>
					        <br>
					          <form action='<?php echo base_url()?>index.php/BuyProductController' method='POST'>		
					          	<input type="hidden" name="kode_item" value="<?php echo $row['kode_item']; ?>">
					          	<!--<input type="hidden" name="kode_merchant" value="<?php echo $row['kode_merchant']; ?>">!-->
					          	<input type="hidden" name="nama_item" value="<?php echo $row['nama_item']; ?>">
					          	<input type="hidden" name="berat" value="<?php echo $row['berat']; ?>">
					          	<input type="hidden" name="itemsell_desc" value="<?php echo $row['itemsell_desc']; ?>">
					          	<input type="hidden" name="harga" value="<?php echo $row['harga']; ?>">
					          	<input type="hidden" name="jumlah_terjual" value="<?php echo $row['jumlah_terjual']; ?>">
					          	<input type="hidden" name="picture_link" value="<?php echo $row['picture_link']; ?>">
					          	<button class='btn btn-info btn-block' name='btnBuy' type="Submit">
					          		BUY
					          	</button>
					          </form>
					</div>
				  </div>
				</li>
				
	<?php  } ?>
<!-- </table> -->
	</ul>
</div>
	</div>
</div>


</body>
<?php 
	echo $footer;
	 ?>
</html>