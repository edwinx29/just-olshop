<!DOCTYPE html>
<html>
<head>
	<?php
		echo $js;
		echo $css;
		echo $header
	?>
	<title>Upload Bukti Transfer</title>
</head>
<body>
	<div id="mainBody">
		<div class="container">

			<form class="span9 form-group" id="emp_form" action="<?php echo base_url()?>index.php/CheckoutController/upload" method="POST" style="padding-left:100px;" enctype="multipart/form-data">
				<div class="form-group">
					<p>Pembayaran melalui transfer dapat dilakukan dengan melakukan transfer ke rekening berikut:
						<br>BCA<br> 0073481063 / Muhammad Arief Widyanto </p>
				</div>


				<div class="form-group">
					<label class="control-label col-sm-2" for="username">Nomor Rekening</label>
					<div class="col-sm-10 ">
						<input type="text" class="form-control" name="no_rek" placeholder="Nomor Rekening" size="40" required="true"></input>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-2" for="username">Bank</label>
					<div class="col-sm-10 ">
						<input type="text" class="form-control" name="bank" placeholder="Bank" size="40" required="true"></input>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-2" for="password">Nama Pemilik Rekening</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="nama_pemilik" placeholder="Nama Pemilik Rekening" required="true" size="40"></input>
					</div>
				</div>

					<table cellspacing="0" width="100%" class="display table table-striped table-bordered">
						<thead>
							<th>UPLOAD BUKTI TRANSFER <?php echo $kode_trans; ?></th>
							
						</thead>
						<tr>
							<td>
								<input type="file" name="fileToUpload" id="fileToUpload">
							</td>
						</tr>
					</table>
					<input type="hidden" name="kode_trans" value="<?php echo $kode_trans; ?>">
				
					<?php if(isset($error)){
						echo '<h4>'.$error.'<h4>';
					} ?>

				<button  style="padding-right:20px;" name='btnBuy' type="Submit"class='btn btn-large btn-success'><span>Konfirmasi</span></button>
				<a href="<?php echo base_url()?>index.php/HomeController" role="button" data-toggle="modal" style="padding-right:0"><span class="btn btn-large btn-danger">Upload Nanti</span></a>
			</form>
		</div>
	</div>
</body>
	<?php 
		echo $footer
	?>
</html>