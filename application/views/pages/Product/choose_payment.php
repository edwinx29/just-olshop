<!DOCTYPE html>
<html>
<head>
	<?php
		echo $js;
		echo $css;
		echo $header
	?>
	<title>Payment Method</title>
</head>
<body>
	<div id="mainBody">
		<div class="container">

			<form class="span9 form-group" id="emp_form" action="<?php echo base_url()?>index.php/CheckoutController/cek" method="POST" style="padding-left:100px;">
				<div class="form-group">
					<label class="control-label col-sm-2" for="username">Nama Penerima   : <?php echo $kode_trans;?></label>
				</div>

				<div class="form-group">
					<label class="control-label col-sm-2" for="password">Alamat Penerima : <?php echo $alamat_penerima;?></label>
					
				</div>

				<div class="form-group">
					<label class="control-label col-sm-2" for="username">Kurir           : <?php echo $kurir;?></label>
					
				</div>

				
					<table cellspacing="0" width="100%" class="display table table-striped table-bordered">
						<thead>
							<th>Nama Barang</th>
							<th>Qty</th>
							<th>Harga</th>
							<th>Total Harga</th>
							<th>Pay by Poin</th>
						</thead>
						<tr>
							<td><?php echo $nama_item;?></td>
							<td><?php echo $qty;?></td>
							<td><?php echo $harga;?></td>
							<td><?php echo $harga*$qty;?></td>
							<td><?php if($harga<1000000){echo 1000*$qty;} else {echo 2500*$qty;}?></td>
						</tr>
					</table>
					<h2>Your Point : <?php echo $_SESSION['poin'];?></h2>
					<input type="hidden" name="kode_item" value="<?php echo $kode_item; ?>">
					<input type="hidden" name="kode_trans" value="<?php echo $kode_trans; ?>">
					<input type="hidden" name="nama_item" value="<?php echo $nama_item; ?>">
					<input type="hidden" name="harga" value="<?php echo $harga; ?>">
					<input type="hidden" name="qty" value="<?php echo $qty; ?>">
					<input type="hidden" name="nama_penerima" value="<?php echo $nama_penerima; ?>">
					<input type="hidden" name="alamat_penerima" value="<?php echo $alamat_penerima; ?>">
					<input type="hidden" name="kurir" value="<?php echo $kurir; ?>">
				
					<?php if(isset($error)){
						echo '<h4>'.$error.'<h4>';
					} ?>
					<input type="submit" name="poin_button" value="Pay by Gift Point" class="btn btn-large btn-success"/>
					<input type="submit" name="trf_button" value="Pay by Transfer" class="btn btn-large btn-success"/>
				
			</form>
		</div>
	</div>
</body>
	<?php 
		echo $footer
	?>
</html>