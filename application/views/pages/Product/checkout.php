<!DOCTYPE html>
<html>
<head>
	<?php
		echo $js;
		echo $css;
		echo $header
	?>
	<title>Transaction Data</title>
</head>
<body>
<div id="mainBody">
	<div class="container">
	      	<form class="span9 form-group" id="emp_form" action="<?php echo base_url()?>index.php/CheckoutController/add" method="POST" style="padding-left:100px;">

			<div class="form-group">
					<label class="control-label col-sm-2" for="username">Nama Penerima</label>
					<div class="col-sm-10 ">
						<input type="text" class="form-control" name="nama_penerima" placeholder="Nama Penerima" size="40" required="true"></input>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-2" for="password">Alamat Penerima</label>
					<div class="col-sm-10">
						<textarea class="form-control" name="alamat_penerima" placeholder="Alamat Penerima" required="true" rows="6" cols="30"></textarea>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-2" for="username">Kurir</label>
					<div class="col-sm-10 ">
		 				<select class="text" name="kurir">
							<option>JNE</option>
							<option>DHL</option>
							<option>J&T Express</option>
							<option>Tiki</option>
							<option>Si Cepat</option>
						</select> 
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-2" for="username">Qty Item</label>
					<div class="col-sm-10 ">
						<input type="text" class="form-control" name="qty" placeholder="Quantity Item" size="40" required="true"></input>
					</div>
				</div>
		

			
				<div class="form-group">
								<input type="hidden" name="kode_item" value="<?php echo $kode_item; ?>">
					          	<input type="hidden" name="nama_item" value="<?php echo $nama_item; ?>">
					          	<input type="hidden" name="harga" value="<?php echo $harga; ?>">
					          	<div>
					          	<button  style="padding-right:20px;" name='btnBuy' type="Submit"class='btn btn-large btn-success'><span>Confirm</span>
					          	</button>
								<a href="<?php echo base_url()?>index.php/ProductController/cancel"  role="button" data-toggle="modal" style="padding-right:0" value="cancel"><span class="btn btn-large btn-danger">Cancel</span></a>
								</div>
				</div>
				</form>	
	
		</div>
	</div>
</body>
	<?php 
		echo $footer
	?>
</html>