 <link rel="shortcut icon" href="themes/images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="themes/images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="themes/images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="themes/images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="themes/images/ico/apple-touch-icon-57-precomposed.png">
	<style type="text/css" id="enject"></style>
<div class="container">
<!-- Navbar ================================================== -->
<div id="logoArea" class="navbar" >
<a id="smallScreen" data-target="#topMenu" data-toggle="collapse" class="btn btn-navbar">
	<span class="icon-bar"></span>
	<span class="icon-bar"></span>
	<span class="icon-bar"></span>
</a>
  <div class="navbar-inner" width="100">
    <a class="brand" href="<?php echo base_url();?>index.php/HomeController"><img src="<?php echo base_url();?>themes/images/logo.png" alt="Bootsshop"/></a>
		
   
    <?php if(isset($_SESSION['email'])&&$_SESSION['email']=="ADMIN"){ ?>
    <ul id="topMenu" class="nav pull-right">
    <li class="">
	 <a href="<?php echo base_url();?>index.php/ConnectController/logout" role="button" data-toggle="modal" style="padding-right:0"><span class="btn btn-large btn-success">Log Out</span></a>
	</li>
	 <?php }else if(isset($_SESSION['email'])&&$_SESSION['email']!="ADMIN"){ ?>
	 	 <ul id="topMenu" class="nav pull-right">
	<li class=""><a href="<?php echo base_url();?>index.php/ProjectsController">Projects</a></li>
	 <li class=""><a href="<?php echo base_url();?>index.php/ProductController">Gifts</a></li>
	<li class=""><a href="<?php echo base_url();?>index.php/ProfileController">My Account</a></li>

	 <li class="">
	 <a href="<?php echo base_url();?>index.php/ConnectController/logout" role="button" data-toggle="modal" style="padding-right:0"><span class="btn btn-large btn-success">Log Out</span></a>
	</li>

	 <?php }
	 else { ?>
	 	 <ul id="topMenu" class="nav pull-right">
	<li class=""><a href="<?php echo base_url();?>index.php/ProjectsController">Projects</a></li>
	 <li class=""><a href="<?php echo base_url();?>index.php/ProductController">Gifts</a></li>
	  <li class="">
	 <a href="<?php echo base_url();?>index.php/SignUpController" role="button" data-toggle="modal" style="padding-right:0"><span class="btn btn-large btn-success">Sign Up</span></a>
	</li>

	 <li class="">
	 <a href="<?php echo base_url();?>index.php/ConnectController" role="button" data-toggle="modal" style="padding-right:0"><span class="btn btn-large btn-success">Login</span></a>
	</li>
	<?php } ?>

    </ul>
  </div>