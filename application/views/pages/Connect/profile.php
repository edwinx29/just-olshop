<!DOCTYPE html>
<html>
<head>
	<?php
		echo $js;
		echo $css;
	?>
	<title>My Profile</title>
</head>
<body>
	<?php
		echo $header;
	?>

<div id="mainBody">
	<div class="container">

		<a href="<?php echo base_url()?>index.php/EditProfileController">
			<button type="button" class="btn btn-primary pull-right">
				Edit Profile
			</button>
		</a>
		<h3>Profile</h3>

		<table>
			<tr>
				<th>Nama</th>
				<th></th>
				<th><?php echo $_SESSION['name'];?></th>
			</tr>

			<tr>
				<th>Email</th>
				<th></th>
				<th><?php echo $_SESSION['poin'];?></th>
			</tr>
		</table>
	</div>
</div>
</body>
<?php
		echo $footer;
	?>
</html>