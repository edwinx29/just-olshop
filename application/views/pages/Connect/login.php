<!DOCTYPE html>
<html>
<head>
	<?php
		echo $js;
		echo $css;
	?>
	<title>Account Login</title>
</head>
<body>
	<?php
		echo $header;
	?>
<div id="mainBody">
	<div class="container">
	      	<form id="loginForm" style="width:30%; margin-right:auto; margin-left:auto;" action="<?php echo base_url()?>index.php/ConnectController/login" method="POST">
	      		<h4>Account Login</h4>
	      		
			  	<div class="form-group">
			    	<label for="email">Email:</label>
			    	<input type="text" class="form-control" id="email" name="email"><?php if (isset($msg1)){
					    echo "<div class='error'>".$msg1."</div>";
					}?>
			  	</div>
			  	<div class="form-group">
			    	<label for="password">Password:</label>
			   		<input type="password" class="form-control" id="password" name="password"><?php if (isset($msg2)){
					    echo "<div class='error'>".$msg2."</div>";
					}?>
			  	</div>

				<div class="form-group">
		        	<div style="text-align: center;">
		            	<p id="errormsg"></p>
		            	</div>
		        </div>

				<div class="form-group">
			  		<button type="submit" class="btn btn-default btn-primary" name="signin">Submit</button>
		    	</div>
		    	<?php if (isset($error)){
					    echo "<div class='form-group'><div class='error'>".$error."</div></div>";
					}?>
			</form>
	</div>
</div>
</body>
<?php
	echo $footer;
?>
</html>