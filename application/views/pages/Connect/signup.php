<!DOCTYPE html>
<html>
<head>
	<?php
		echo $js;
		echo $css;
	?>
	<title>Sign Up</title>
</head>
<body>
	<?php
		echo $header;
	?>
	<div id="mainBody">
		<div class="container" style="width:30%; margin-right:auto; margin-left:auto;">
	      	<form class="loginForm" id="emp_form"  action="<?php echo base_url()?>index.php/SignUpController/addUser" method="POST">
	      		<h4>Create New Account</h4>
				<div class="form-group">
					<label for="email">Email </label>
					<input type="text" class="form-control" name="email" placeholder="Email" size="40" required="true">
				</div>
				<div class="form-group">
					<label for="password">Password </label>
					<input type="password" class="form-control" name="password" placeholder="Password" size="40" required="true">
				</div>
				<div class="form-group">
					<label for="username">Nama </label>
					<input type="text" class="form-control" name="nama" placeholder="Nama" size="40" required="true">
				</div>
				<div class="form-group">
					<label for="tanggalLahir">Tanggal Lahir </label>
					<input type="date" class="form-control" name="tgllahir" placeholder="TanggalLahir" size="40" required="true">
				</div>
				<div class="form-group">
					<label for="kodePos">Kode Pos </label>
					<input type="text" class="form-control" name="kodepos" placeholder="KodePos" size="40" required="true">
				</div>
				<div class="form-group col-xs-7">
					<input type="submit" class="btn btn-primary" name="submit" value="Add" >
					<a href="<?php echo base_url()?>index.php/SignUpController" class="btn btn-danger" name="cancel" value="Cancel">Cancel</a>
				</div>
				<?php if (isset($error)){
					    echo "<div class='form-group'><div class='error'>".$error."</div></div>";
					}?>
			</form>
		</div>
	</div>
	
</body>
	<?php 
		echo $footer;
	?>
</html>