<!DOCTYPE html>
<html>
<head>
	<title>Add New Product</title>
	<?php 
	echo $js;
	echo $css;
	echo $header;
	 ?>
</head>
<body>
<div id="mainBody">
	<div class="container">
	      	<form class="span9 form-group" id="item_form" action="<?php echo base_url()?>index.php/AddProductController/add" method="post" style="padding-left:100px;">
				<div class="form-group">
					<label class="control-label col-sm-2" for="username">Nama Item</label>
					<div class="col-sm-10 ">
						<input type="text" class="form-control" name="nama_item" placeholder="Nama Item" size="40" required="true"></input>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-sm-2" for="password">Berat dalam KG</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="berat" placeholder="Berat" size="40" required="true"></input>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-sm-2" for="fullname">Deskripsi Barang</label>
					<div class="col-sm-10">
						<textarea class="form-control" name="itemsell_desc" placeholder="Deskripsi Barang" cols="30" rows="6" required="true"></textarea>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-sm-2" for="fullname">Harga Barang</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="harga" placeholder="Harga Barang" size="40" required="true"></input>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-sm-2" for="fullname">Link Gambar</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="picture_link" placeholder="Link Gambar" size="40" required="true"></input>
					</div>
				</div>

				<div class="form-group">
					<button type="submit" class="btn btn-large btn-primary" name="submit" value="Add" ><span>Add</span></button>
					<a href="<?php echo base_url()?>index.php/HomeController"  name="cancel" value="Cancel"><span class="btn btn-large btn-danger">Cancel</span></a>
				</div>
			</form>
		</div>
	</div>
</body>
<?php 
	echo $footer;
 ?>
</html>

