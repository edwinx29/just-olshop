<!DOCTYPE html>
<html>
<head>
	<?php
		echo $js;
		echo $css;
		echo $header
	?>
	<title>Projects List</title>
</head>
<body>
	<div id="mainBody">
		<div class="container">
		<div>
		<h3>Projects List <?php if(isset($_SESSION['email'])){ ?> <a href="<?php echo base_url();?>index.php/ProjectsController/myProject" role="button" data-toggle="modal" style="float: right;padding-right: 50px;"><span class="btn btn-large btn-success">My Project</span></a><?php } ?></h3>
		<?php 
			echo $table_projects
		?>
		 <?php if($button!=null){ ?>
		 <a href="<?php echo base_url();?>index.php/AddProjectController" role="button" data-toggle="modal" style="padding-right:0"><span class="btn btn-large btn-success">Add Project</span></a>
		<?php }?>

		</div>
	</div>	
</body>
	<?php 
		echo $footer
	?>
</html>