<div class="container">
		
	
		</div>
		<table id="projects" cellspacing="0" width="100%" class="display table table-striped table-bordered">
			<thead>
				<th>No</th>
				<th>Nama Project</th>
				<th>Project Description</th>
				<th>Due Date</th>
				<th>Person Joined</th>
				<th>Status</th>
				<th>Action</th>
			</thead>
			<tbody>
			<?php foreach($arr as $key=>$value): ?>
				<tr>
						<td><?php echo $key + 1 ?></td>
						<?php $id = $value['kode_project'];
						$query = $this->db->query('SELECT * FROM kaloborators_transaction WHERE kode_project ="'.$id.'"');
						$counts = $query->num_rows();?>
						<td><?php echo $value['nama_project']; ?></td>
						<td><?php echo $value['project_desc']; ?></td>
						<td><?php echo $value['due_date']; ?></td>
						<td><?php echo $counts."/".$value['required_person']?></td>
						<td><?php echo $value['status_project']; ?></td>
						<td><form action='<?php echo base_url()?>index.php/AddProjectController/update' method='POST'>
							<input type='hidden' name='kode_project' value="<?php echo $value['kode_project']; ?>">
							<input type='hidden' name='nama_project' value="<?php echo $value['nama_project']; ?>">
							<input type='hidden' name='project_desc' value="<?php echo $value['project_desc']; ?>">
							<input type='hidden' name='kode_project' value="<?php echo $value['kode_project']; ?>">
							<input type='hidden' name='required_person' value="<?php echo $value['required_person']; ?>">
							<input type='hidden' name='due_date' value="<?php echo $value['due_date']; ?>">
							<input type='hidden' name='status_project' value="<?php echo $value['status_project']; ?>">
        						<button class='btn btn-info btn-block' name='btnUpdate' type='submit' <?php if($value['status_project']=='DONE' || $value['status_project']=="EXPIRED"){echo "disabled";}?>>
        							 Selesai
        						</button>
        					
        					</form>
        				</td>
				</tr>
			<?php endforeach; ?>
			</tbody>
		</table>

		<script type="text/javascript">
			$(document).ready(function() {
			    $('#projects').DataTable();
			} );
		</script>
</div>