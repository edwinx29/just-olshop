<!DOCTYPE html>
<html>
<head>
	<?php
		echo $js;
		echo $css;
		echo $header
	?>
	<title><?php echo $nama_project; ?></title>
</head>
<body>
		<div class="container">

		<h2>Confirmation</h2>
		<form class="form-horizontal" id="emp_form" action="<?php echo base_url()?>index.php/ProjectsController/onaction" method="POST">
		<table id="projects" cellspacing="0" width="100%" class="display table table-striped table-bordered">
			<tr>
				<th>Nama Project</th>
				<th><?php echo $nama_project; ?></th>
			</tr>
			<tr>
				<th>Deskripsi Project</th>
				<th><?php echo $project_desc; ?></th>
			</tr>
			<tr>
				<th>Batas Akhir</th>
				<th><?php echo $due_date; ?></th>
			</tr>
			<tr>
				<th>Status Project</th>
				<th><?php echo $status_project; ?></th>
			</tr>
			<tr>
				<th>Collaborator List</th>
				<th><?php echo $table_collab; ?></th>
			</tr>
			<?php 
			$query = $this->db->query('SELECT * FROM kaloborators_transaction WHERE kode_project ="'.$kode_project.'"');
			$count = $query->num_rows();
			if($count==$required_person){
				echo "</table>"; ?>
				 <div class='form-group col-xs-7'>
					    <div class='error' style="color:red;">Quota Already Reach Maximum Capacity</div>
						<input type="button" onclick="location.href='<?php echo base_url()?>index.php/ProjectsController/cancel';" class="btn btn-danger" value="Back"></input>
				</div> <?php }
				else if($count<$required_person){ ?>
				</table>
				<?php if (isset($error)){ ?>
						    <div class='form-group col-xs-7'>
						    	<div class='error' style="color:red;"><?php echo $error; ?></div>
								<input type="button" onclick="location.href='<?php echo base_url()?>index.php/ProjectsController/cancel';" class="btn btn-danger" value="Back"></input>
						    </div>
				<?php } else {?>
							<div class="form-group col-xs-7">
							<input type='hidden' name='kode_project' value="<?php echo $kode_project; ?>">
							<input type="submit" class="btn btn-primary" name="submit" value="OK" ></input>
							<input type="button" onclick="location.href='<?php echo base_url()?>index.php/ProjectsController/cancel';" class="btn btn-danger" value="Back"></input>
							</div>
				<?php }}?>
		</form>

		<script type="text/javascript">
			$(document).ready(function() {
			    $('#projects').DataTable();
			} );
		</script>
</div>
	
</body>
	<?php 
		echo $footer
	?>
</html>