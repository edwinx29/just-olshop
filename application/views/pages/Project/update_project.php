<!DOCTYPE html>
<html>
<head>
	<?php
		echo $js;
		echo $css;
		echo $header
	?>
	<title>Add New Project</title>
</head>
<body>
<div id="mainBody">
	<div class="container">
	      	<form class="span9 form-group" id="emp_form" action="<?php echo base_url()?>index.php/AddProjectController/updateData" style="padding-left:100px;" method="POST">
				
				<div class="form-group">
					<label class="control-label col-sm-2" for="project_name">Nama Project</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="nama_project" placeholder="Nama_Project" size="40" required="true" value="<?php echo $nama_project;?>"></input>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-sm-2" for="desc">Project Description</label>
					<div class="col-sm-10 ">
						<textarea class="form-control" name="project_desc" placeholder="Project_Desc" required="true" rows="6" cols="30" value="<?php echo $nama_project;?>"></textarea>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-sm-2" for="desc">Deadline</label>
					<div class="col-sm-10 ">
						<input type="date" class="form-control" name="due_date" placeholder="Deadline" size="40" required="true" value="<?php echo $value['nama_project'];?>">
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-sm-2" for="target">Target</label>
					<div class="col-sm-10 ">
						<input type="text" class="form-control" name="target" placeholder="Target" size="40" required="true" value="<?php echo $value['nama_project'];?>"></input>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-sm-2" for="kontak">No Kontak</label>
					<div class="col-sm-10 ">
						<input type="text" class="form-control" name="no_kontak" placeholder="No_Kontak" size="40" required="true" value="<?php echo $value['nama_project'];?>"></input>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-sm-2" for="person">Required Person</label>
					<div class="col-sm-10 ">
						<input type="text" class="form-control" name="required_person" placeholder="Required_Person" size="40" required="true" value="<?php echo $value['nama_project'];?>" ></input>
					</div>
				</div>

				<div class="form-group">
					<button style="padding-right:20px;" name='btnBuy' type="Submit"class='btn btn-large btn-success'><span>Add</span></button>
					<a href="<?php echo base_url()?>index.php/AddProjectController/cancel"  role="button" data-toggle="modal" style="padding-right:0" value="cancel"><span class="btn btn-large btn-danger">Cancel</span></a>
				</div>
			</form>
	</div>
</div>
</body>
	<?php 
		echo $footer
	?>
</html>