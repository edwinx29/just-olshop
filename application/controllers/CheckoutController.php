<?php

defined('BASEPATH') or exit('No direct script access allowed');

class CheckoutController extends CI_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->model('product_model');
		$this->load->model('user_model');
	}

	public function index(){
		$data['kode_item'] = $this->input->post('kode_item');
		$data['kode_merchant'] = $this->input->post('kode_merchant');
		$data['nama_item']  = $this->input->post('nama_item');
		$data['berat'] = $this->input->post('berat');
		$data['itemsell_desc'] = $this->input->post('itemsell_desc');
		$data['harga'] = $this->input->post('harga');
		$data['jumlah_terjual'] = $this->input->post('jumlah_terjual');
		$data['picture_link'] = $this->input->post('picture_link');
		
		$data['js']= $this->load->view('include/js.php',NULL, TRUE);
		$data['css']= $this->load->view('include/css.php',NULL, TRUE);
		$data['header']= $this->load->view('pages/header.php',NULL, TRUE);
		$data['footer']= $this->load->view('pages/footer.php',NULL, TRUE);
		$this->load->view('pages/Product/checkout.php', $data);   	
		// $data['product'] = $this->buy_product_model->getProducts();
	}

	public function add(){
		$kode_item = $this->input->post('kode_item');
		$nama_barang =$this->input->post('nama_item');
		$harga = $this->input->post('harga');
		$nama_penerima = $this->input->post('nama_penerima');
		$alamat_penerima = $this->input->post('alamat_penerima');
		$kurir = $this->input->post('kurir');
		$qty = $this->input->post('qty');
		$email = $_SESSION['email'];
		
		$data['kode_item']=$kode_item;
		$data['nama_item']=$nama_barang;
		$data['qty']=$qty;
		$data['harga']=$harga;
		$data['nama_penerima']=$nama_penerima;
		$data['alamat_penerima']=$alamat_penerima;
		$data['kurir']=$kurir;
		$data['js']= $this->load->view('include/js.php',NULL, TRUE);
		$data['css']= $this->load->view('include/css.php',NULL, TRUE);
		$data['header']= $this->load->view('pages/header.php',NULL, TRUE);
		$data['footer']= $this->load->view('pages/footer.php',NULL, TRUE);

		$data['kode_trans']=$this->product_model->addTransaction($nama_penerima, $alamat_penerima, $kurir, $kode_item, $qty, $harga*$qty, $email);
		$this->load->view('pages/Product/choose_payment.php', $data);
		//redirect('/HomeController');
	}

	public function cek(){
		
		if (isset($_POST['poin_button'])) {
    		$harga = $this->input->post('harga');
		$qty = $this->input->post('qty');
		$poin= $_SESSION['poin'];
		if($harga<1000000){
			if($poin<(1000*$qty)){
				$kode_trans = $this->input->post('kode_trans');
				$kode_item = $this->input->post('kode_item');
				$nama_barang =$this->input->post('nama_item');
				$harga = $this->input->post('harga');
				$nama_penerima = $this->input->post('nama_penerima');
				$alamat_penerima = $this->input->post('alamat_penerima');
				$kurir = $this->input->post('kurir');
				$qty = $this->input->post('qty');
				$email = $_SESSION['email'];
				
				$data['kode_trans']=$kode_trans;
				$data['kode_item']=$kode_item;
				$data['nama_item']=$nama_barang;
				$data['qty']=$qty;
				$data['harga']=$harga;
				$data['nama_penerima']=$nama_penerima;
				$data['alamat_penerima']=$alamat_penerima;
				$data['kurir']=$kurir;
				$data['js']= $this->load->view('include/js.php',NULL, TRUE);
				$data['css']= $this->load->view('include/css.php',NULL, TRUE);
				$data['header']= $this->load->view('pages/header.php',NULL, TRUE);
				$data['footer']= $this->load->view('pages/footer.php',NULL, TRUE);
				$data['error']="Poin Anda Kurang";

				$this->load->view('pages/Product/choose_payment.php', $data);
			}
			else{
				$_SESSION['poin']=$poin-(1000*$qty);
				$this->user_model->minPoin($_SESSION['email'],$_SESSION['poin']);
				redirect('/HomeController');
			}
		} else{
			if($poin<(2500*$qty)){
				$kode_trans = $this->input->post('kode_trans');
				$kode_item = $this->input->post('kode_item');
				$nama_barang =$this->input->post('nama_item');
				$harga = $this->input->post('harga');
				$nama_penerima = $this->input->post('nama_penerima');
				$alamat_penerima = $this->input->post('alamat_penerima');
				$kurir = $this->input->post('kurir');
				$qty = $this->input->post('qty');
				$email = $_SESSION['email'];
				
				$data['kode_trans']=$kode_trans;
				$data['kode_item']=$kode_item;
				$data['nama_item']=$nama_barang;
				$data['qty']=$qty;
				$data['harga']=$harga;
				$data['nama_penerima']=$nama_penerima;
				$data['alamat_penerima']=$alamat_penerima;
				$data['kurir']=$kurir;
				$data['js']= $this->load->view('include/js.php',NULL, TRUE);
				$data['css']= $this->load->view('include/css.php',NULL, TRUE);
				$data['header']= $this->load->view('pages/header.php',NULL, TRUE);
				$data['footer']= $this->load->view('pages/footer.php',NULL, TRUE);
				$data['error']="Poin Anda Kurang";

				$this->load->view('pages/Product/choose_payment.php', $data);
			}
			else{
				$_SESSION['poin']=$poin-(2500*$qty);
				$this->user_model->minPoin($_SESSION['email'],$_SESSION['poin']);
				redirect('/HomeController');
			}
		}
		} else if (isset($_POST['trf_button'])) {
			$data['js']= $this->load->view('include/js.php',NULL, TRUE);
			$data['css']= $this->load->view('include/css.php',NULL, TRUE);
			$data['header']= $this->load->view('pages/header.php',NULL, TRUE);
			$data['footer']= $this->load->view('pages/footer.php',NULL, TRUE);
			$data['kode_trans']=$this->input->post('kode_trans');
		    $this->load->view('pages/Product/upload_view.php', $data);
		}
	}

	public function upload_view(){
		$data['js']= $this->load->view('include/js.php',NULL, TRUE);
			$data['css']= $this->load->view('include/css.php',NULL, TRUE);
			$data['header']= $this->load->view('pages/header.php',NULL, TRUE);
			$data['footer']= $this->load->view('pages/footer.php',NULL, TRUE);
			$data['kode_trans']=$this->input->post('kode_transaksi');
		    $this->load->view('pages/Product/upload_view.php', $data);
	}

	public function upload(){
		$data['js']= $this->load->view('include/js.php',NULL, TRUE);
		$data['css']= $this->load->view('include/css.php',NULL, TRUE);
		$data['header']= $this->load->view('pages/header.php',NULL, TRUE);
		$data['footer']= $this->load->view('pages/footer.php',NULL, TRUE);
		$data['kode_trans']=$this->input->post('kode_trans');
		$name=$this->product_model->getImageName();
		$data['no_rek']=$this->input->post('no_rek');
		$data['bank']=$this->input->post('bank');
		$data['nama_pemilik']=$this->input->post('nama_pemilik');

		$target_dir = "gambar/";
		$uploadOk = 1;
		$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
		$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
		$target_file = $target_dir . $name.$imageFileType;
		// Check if image file is a actual image or fake image
		
		// Allow certain file formats
		if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
		&& $imageFileType != "gif" ) {
		    $uploadOk = 0;
		}
		// Check if $uploadOk is set to 0 by an error
		if ($uploadOk == 0) {
		    $data['error'] = "Sorry, your file was not uploaded.";
		    $this->load->view('pages/Product/upload_view.php', $data);
		// if everything is ok, try to upload file
		} else {
		    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
		    	$this->product_model->addBukti($_SESSION['email'],$data['kode_trans'],$data['nama_pemilik'], $data['no_rek'],$data['bank'],$name);
		    	$this->product_model->updateTransaction($data['kode_trans'],"WAITING FOR CONFIRMATION");
		        redirect('/HomeController');
		    } else {
		        $data['error'] = "Sorry, there was an error uploading your file.";
		        $this->load->view('pages/Product/upload_view.php', $data);
		    }
		}
	}


}
?>