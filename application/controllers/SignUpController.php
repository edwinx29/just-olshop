<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SignUpController extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->model('user_model');
	}
	public function index(){
		$data['js']= $this->load->view('include/js.php',NULL, TRUE);
		$data['css']= $this->load->view('include/css.php',NULL, TRUE);
		$data['header']= $this->load->view('pages/header.php',NULL, TRUE);
		$data['footer']= $this->load->view('pages/footer.php',NULL, TRUE);
		$this->load->view('pages/Connect/signup.php',$data);
	
	}

	public function addUser(){
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$fullname = $this->input->post('nama');
		$tanggal_lahir = $this->input->post('tgllahir');
		$kode_pos = $this->input->post('kodepos');

		$check_data = $this->user_model->checkData($email);
		if($check_data!=null){
			$data['js']= $this->load->view('include/js.php',NULL, TRUE);
			$data['css']= $this->load->view('include/css.php',NULL, TRUE);
			$data['header']= $this->load->view('pages/header.php',NULL, TRUE);
			$data['footer']= $this->load->view('pages/footer.php',NULL, TRUE);
			$data['error']="Email Already Used";
			$this->load->view('pages/Connect/signup.php',$data);
		}
		else {
		$result = $this->user_model->addEmp($email, $password, $fullname, $tanggal_lahir, $kode_pos);
		/*$this->load->library('email');
 
		$config = array();
		$config['protocol'] = 'smtp';
		$config['smtp_host'] = 'smtp@gmail.com';
		$config['smtp_user'] = 'edwinxkristian@gmail.com';
		$config['smtp_pass'] = '...';
		$config['smtp_port'] = 465;
		$config['mailtype']='html';
		$config['charset']='utf-8';
		
		$this->email->initialize($config);
 
		$this->email->set_newline("\r\n");

		$this->email->from('edwinxkristian.com', 'Gift Code');
		$this->email->to("'".$email."'");
		//$this->email->cc('another@another-example.com');
		//$this->email->bcc('them@their-example.com');

		$this->email->subject('Gift COde Registration');
		$this->email->message('Welcome to Gift Code.');

		$this->email->send();*/
		redirect('/ConnectController');
	}
}

	

	
}
?>
