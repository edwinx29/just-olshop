<?php

defined('BASEPATH') or exit('No direct script access allowed');

class AddProductController extends CI_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->model('product_model');
	}

	public function index(){
		
		$data['js']= $this->load->view('include/js.php',NULL, TRUE);
		$data['css']= $this->load->view('include/css.php',NULL, TRUE);
		$data['header']= $this->load->view('pages/header.php',NULL, TRUE);
		$data['footer']= $this->load->view('pages/footer.php',NULL, TRUE);
		$this->load->view('pages/Admin/add_item.php', $data);   	
		// $data['product'] = $this->buy_product_model->getProducts();
	}

	public function add(){
		$nama_item = $this->input->post('nama_item');
		$berat = $this->input->post('berat');
		$itemsell_desc = $this->input->post('itemsell_desc');
		$harga = $this->input->post('harga');
		$picture_link = $this->input->post('picture_link');

		$result = $this->product_model->add($nama_item, $berat, $itemsell_desc, $harga, $picture_link);
		redirect('/HomeController');
	}

	public function search(){

	}


         
	



}
?>