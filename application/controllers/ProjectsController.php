<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProjectsController extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->model('projects_model');

	}

	public function index(){
		$data['js']= $this->load->view('include/js.php',NULL, TRUE);
		$data['css']= $this->load->view('include/css.php',NULL, TRUE);
		$data['header']= $this->load->view('pages/header.php',NULL, TRUE);
		$data['footer']= $this->load->view('pages/footer.php',NULL, TRUE);
		$data['button']=null;
		$proj['arr'] = $this->projects_model->getProj();
		$data['table_projects'] = $this->load->view('pages/Project/table_projects.php', $proj, TRUE);
		$this->load->view('pages/Project/projects.php',$data);
	}

	public function action(){
		if(isset($_SESSION['email'])){
		$email=$_SESSION['email'];
		$nama_project = $this->input->post('nama_project');
		$project_desc = $this->input->post('project_desc');
		$kode_project = $this->input->post('kode_project');
		$required_person = $this->input->post('required_person');
		$due_date = $this->input->post('due_date');
		$status_project = $this->input->post('status_project');

		$data['nama_project'] = $nama_project;
		$data['project_desc'] = $project_desc;
		$data['kode_project'] = $kode_project;
		$data['status_project'] = $status_project;
		$data['required_person']= $required_person;
		$data['due_date']= $due_date;
		$check = $this->projects_model->checkCollab($email,$kode_project);
		if($check!=null){
			$collab['x']=$this->projects_model->getAllCollabs($kode_project);

			$data['js']= $this->load->view('include/js.php',NULL, TRUE);
			$data['css']= $this->load->view('include/css.php',NULL, TRUE);
			$data['header']= $this->load->view('pages/header.php',NULL, TRUE);
			$data['footer']= $this->load->view('pages/footer.php',NULL, TRUE);
			$proj['arr'] = $this->projects_model->getProj();
			$data['table_projects'] = $this->load->view('pages/Project/table_projects.php', $proj, TRUE);
			$data['error']="You already take this project";
			$data['table_collab'] = $this->load->view('pages/Project/table_collab.php', $collab, TRUE);
			$this->load->view('pages/Project/confirmation.php',$data);
			}
		else{
			$collab['x']=$this->projects_model->getAllCollabs($kode_project);
			
			$data['js']= $this->load->view('include/js.php',NULL, TRUE);
			$data['css']= $this->load->view('include/css.php',NULL, TRUE);
			$data['header']= $this->load->view('pages/header.php',NULL, TRUE);
			$data['footer']= $this->load->view('pages/footer.php',NULL, TRUE);
			$proj['arr'] = $this->projects_model->getProj();
			$data['table_projects'] = $this->load->view('pages/Project/table_projects.php', $proj, TRUE);
			$data['table_collab'] = $this->load->view('pages/Project/table_collab.php', $collab, TRUE);
			$this->load->view('pages/Project/confirmation.php',$data);
			}
		}
		else
			redirect('/ConnectController');
	}



	public function onaction(){
		$kode_project = $this->input->post('kode_project');
		$email_user = $_SESSION['email'];

		$result = $this->projects_model->addCollab($kode_project, $email_user);
		redirect('/ProjectsController');
		
	}

	public function myProject(){
		$data['js']= $this->load->view('include/js.php',NULL, TRUE);
		$data['css']= $this->load->view('include/css.php',NULL, TRUE);
		$data['header']= $this->load->view('pages/header.php',NULL, TRUE);
		$data['footer']= $this->load->view('pages/footer.php',NULL, TRUE);
		$data['button']="YES";
		$proj['arr'] = $this->projects_model->getProjwithEmail($_SESSION['email']);
		$data['table_projects'] = $this->load->view('pages/Project/table_myprojects.php', $proj, TRUE);
		$this->load->view('pages/Project/projects.php',$data);
	}

	public function cancel(){
		redirect('/ProjectsController');
	}


}
?>