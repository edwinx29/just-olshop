<?php

defined('BASEPATH') or exit('No direct script access allowed');

class ProductController extends CI_Controller{
	public function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->model('product_model');
	}
	public function index(){
		$data['product'] = $this->product_model->getProducts2();
		$data['js']= $this->load->view('include/js.php',NULL, TRUE);
		$data['css']= $this->load->view('include/css.php',NULL, TRUE);
		$data['header']= $this->load->view('pages/header.php',NULL, TRUE);
		$data['footer']= $this->load->view('pages/footer.php',NULL, TRUE);

         $this->load->view('pages/Product/product.php', $data);   	
	}

	public function myOrder(){
		$prod['arr'] = $this->product_model->getMyOrder($_SESSION['email']);
		$data['js']= $this->load->view('include/js.php',NULL, TRUE);
		$data['css']= $this->load->view('include/css.php',NULL, TRUE);
		$data['header']= $this->load->view('pages/header.php',NULL, TRUE);
		$data['footer']= $this->load->view('pages/footer.php',NULL, TRUE);
		$data['table_myorder'] = $this->load->view('pages/table_myorder.php', $prod, TRUE);
		$this->load->view('pages/Product/vieworder.php',$data);
	}

	public function cancel(){
		redirect('/ProductController');
	}

}
?>