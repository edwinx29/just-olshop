<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LaporanTransaksiController extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->model('product_model');

	}

	public function index(){
		$data['js']= $this->load->view('include/js.php',NULL, TRUE);
		$data['css']= $this->load->view('include/css.php',NULL, TRUE);
		$data['header']= $this->load->view('pages/header.php',NULL, TRUE);
		$data['footer']= $this->load->view('pages/footer.php',NULL, TRUE);
		$proj['arr'] = $this->product_model->getTransaction();
		$data['table_laporan'] = $this->load->view('pages/table_laporan.php', $proj, TRUE);
		$this->load->view('pages/Admin/laporan_transaksi.php',$data);
	}

	public function update(){
		$data['kode_trans']=$this->input->post('kode_transaksi');
		$data['status']=$this->input->post('status');
		if($data['status']=="WAITING FOR CONFIRMATION"){
			$this->product_model->updateTransaction($data['kode_trans'],"SENDING");
		}
		else if($data['status']=="SENDING"){
			$this->product_model->updateTransaction($data['kode_trans'],"SENT");
		}
		

		$data['js']= $this->load->view('include/js.php',NULL, TRUE);
		$data['css']= $this->load->view('include/css.php',NULL, TRUE);
		$data['header']= $this->load->view('pages/header.php',NULL, TRUE);
		$data['footer']= $this->load->view('pages/footer.php',NULL, TRUE);

		$proj['arr'] = $this->product_model->getTransaction();
		$data['table_laporan'] = $this->load->view('pages/table_laporan.php', $proj, TRUE);

		//echo $data['status'];
		$this->load->view('pages/Admin/laporan_transaksi.php',$data);
	}

	public function allProduct(){
		$data['js']= $this->load->view('include/js.php',NULL, TRUE);
		$data['css']= $this->load->view('include/css.php',NULL, TRUE);
		$data['header']= $this->load->view('pages/header.php',NULL, TRUE);
		$data['footer']= $this->load->view('pages/footer.php',NULL, TRUE);
		$proj['arr'] = $this->product_model->getProducts2();
		$data['table_product'] = $this->load->view('pages/table_product.php', $proj, TRUE);
		$this->load->view('pages/Admin/all_product.php',$data);
	}
}
?>