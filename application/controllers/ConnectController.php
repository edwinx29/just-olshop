<?php

defined('BASEPATH') or exit('No direct script access allowed');
	
class ConnectController extends CI_Controller{
	public function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->model('user_model');
	}

	public function index(){
		$data['js']= $this->load->view('include/js.php',NULL, TRUE);
		$data['css']= $this->load->view('include/css.php',NULL, TRUE);
		$data['header']= $this->load->view('pages/header.php',NULL, TRUE);
		$data['footer']= $this->load->view('pages/footer.php',NULL, TRUE);
		$this->load->view('pages/Connect/login.php',$data);
	}

	public function login(){
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		if(isset($email) && isset($password)){
			if($email=="ADMIN" && $password=="ADMIN"){
				$this->session->set_userdata('email', $email);
				redirect('/LaporanTransaksiController');
			}
			else{
					$data = $this->user_model->login($email, $password);
					if($data!=null){
						$this->session->set_userdata('email', $data->email);
						$this->session->set_userdata('name', $data->nama);
						$this->session->set_userdata('tanggalLahir', $data->tgl_lahir);
						$this->session->set_userdata('kodePos', $data->kode_pos);
						$this->session->set_userdata('poin', $data->gift_point);
						redirect('/HomeController');
					}
					else{
						$data['js']= $this->load->view('include/js.php',NULL, TRUE);
						$data['css']= $this->load->view('include/css.php',NULL, TRUE);
						$data['header']= $this->load->view('pages/header.php',NULL, TRUE);
						$data['footer']= $this->load->view('pages/footer.php',NULL, TRUE);
						$data['error']="Username and Password invalid";
						$this->load->view('pages/Connect/login.php',$data);
					}
				}
			
		}
	}
	
	public function logout(){
		$this->session->unset_userdata('email');
		$this->session->unset_userdata('name');
		$this->session->unset_userdata('tanggalLahir');
		$this->session->unset_userdata('kodePos');
		$this->session->unset_userdata('poin');
		$this->session->sess_destroy();
		redirect('/ConnectController');
	}
}
?>