<?php

defined('BASEPATH') or exit('No direct script access allowed');
	
class ProfileController extends CI_Controller{
	public function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->model('user_model');
	}
	public function index(){
		$data['js']= $this->load->view('include/js.php',NULL, TRUE);
		$data['css']= $this->load->view('include/css.php',NULL, TRUE);
		$data['header']= $this->load->view('pages/header.php',NULL, TRUE);
		$data['footer']= $this->load->view('pages/footer.php',NULL, TRUE);
		$this->load->view('pages/Connect/mydata.php',$data);
	}
}

?>