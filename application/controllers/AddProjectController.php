<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AddProjectController extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->model('projects_model');

	}

	public function index(){
		$data['js']= $this->load->view('include/js.php',NULL, TRUE);
		$data['css']= $this->load->view('include/css.php',NULL, TRUE);
		$data['header']= $this->load->view('pages/header.php',NULL, TRUE);
		$data['footer']= $this->load->view('pages/footer.php',NULL, TRUE);
		$this->load->view('pages/Project/addproject.php',$data);

	
	}

	public function add(){
		$email_user = $_SESSION['email'];
		$nama_project = $this->input->post('nama_project');
		$project_desc = $this->input->post('project_desc');
		$target = $this->input->post('target');
		$date=$this->input->post('due_date');
		$no_kontak = $this->input->post('no_kontak');
		$required_person = $this->input->post('required_person');

		$result = $this->projects_model->addProj($email_user, $nama_project, $project_desc, $target, $no_kontak, $required_person,$date);
		redirect('/ProjectsController');

		
	}

	public function update(){
		$email_user = $_SESSION['email'];
		$kode_project = $this->input->post('kode_project');
		$data['js']= $this->load->view('include/js.php',NULL, TRUE);
		$data['css']= $this->load->view('include/css.php',NULL, TRUE);
		$data['header']= $this->load->view('pages/header.php',NULL, TRUE);
		$data['footer']= $this->load->view('pages/footer.php',NULL, TRUE);
		//$this->load->view('pages/Project/update_project.php',$data);
		$this->projects_model->endProject($kode_project);
		redirect('/ProjectsController');
	}

	public function cancel(){
		redirect('/ProjectsController');
	}
	
}

?>
