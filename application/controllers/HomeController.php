<?php

defined('BASEPATH') or exit('No direct script access allowed');

class HomeController extends CI_Controller{
	public function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->model('user_model');
		$this->load->model('product_model');
		$this->load->model('projects_model');
	}

	public function index(){
		$this->projects_model->updateProject();
		$data['product'] = $this->product_model->getProducts();
		$data['js']= $this->load->view('include/js.php',NULL, TRUE);
		$data['css']= $this->load->view('include/css.php',NULL, TRUE);
		$data['header']= $this->load->view('pages/header.php',NULL, TRUE);
		$data['footer']= $this->load->view('pages/footer.php',NULL, TRUE);
		$proj['arr'] = $this->projects_model->getProj();
		$data['table_projects'] = $this->load->view('pages/Project/table_projects.php', $proj, TRUE);
		$this->load->view('pages/home.php',$data);
	}


}
?>