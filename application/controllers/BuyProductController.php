<?php

defined('BASEPATH') or exit('No direct script access allowed');

class BuyProductController extends CI_Controller{
	public function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->model('product_model');
	}
	public function index(){
		$data['kode_item'] = $this->input->post('kode_item');
		//$data['kode_merchant'] = $this->input->post('kode_merchant');
		$data['nama_item']  = $this->input->post('nama_item');
		$data['berat'] = $this->input->post('berat');
		$data['itemsell_desc'] = $this->input->post('itemsell_desc');
		$data['harga'] = $this->input->post('harga');
		$data['jumlah_terjual'] = $this->input->post('jumlah_terjual');
		$data['picture_link'] = $this->input->post('picture_link');

		$data['product'] = $this->product_model->getProducts();
		$data['js']= $this->load->view('include/js.php',NULL, TRUE);
		$data['css']= $this->load->view('include/css.php',NULL, TRUE);
		$data['header']= $this->load->view('pages/header.php',NULL, TRUE);
		$data['footer']= $this->load->view('pages/footer.php',NULL, TRUE);
		$this->load->view('pages/Product/buy_item.php', $data);   	

	}

	public function action(){

		$this->load->view('pages/Product/buy_item.php',$data);
	}


         
	



}
?>