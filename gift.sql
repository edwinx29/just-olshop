-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 08, 2018 at 05:41 AM
-- Server version: 10.1.33-MariaDB
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gift`
--

-- --------------------------------------------------------

--
-- Table structure for table `item_sell`
--

CREATE TABLE `item_sell` (
  `kode_item` varchar(255) NOT NULL,
  `nama_item` varchar(255) NOT NULL,
  `berat` int(11) NOT NULL,
  `itemsell_desc` varchar(255) NOT NULL,
  `harga` int(11) NOT NULL,
  `jumlah_terjual` int(11) NOT NULL,
  `picture_link` varchar(255) NOT NULL,
  `upload_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item_sell`
--

INSERT INTO `item_sell` (`kode_item`, `nama_item`, `berat`, `itemsell_desc`, `harga`, `jumlah_terjual`, `picture_link`, `upload_date`) VALUES
('item01', 'Kopi Arabika Aceh Gayo', 10, 'The Gayo highland in Aceh, stretching as long as the back of Mount Barisan on Sumatera island, is one of the top arabica producers in Indonesia.', 65000, 98, 'https://s2.bukalapak.com/img/2383551871/w-1000/Kopi_Arabika_Aceh_Gayo_200_Gram___Bubuk___Biji___Premium_Ara.jpg', '2018-07-07'),
('item010', 'test 2', 3, 'test waw', 1234, 0, 'https://images.formget.com/wp-content/uploads/2014/10/codeigniter_image_upload_content.png', '2018-07-07'),
('item02', 'Iron Man GITD T-Shirt', 50, 'Glow In The Dark T-Shirt', 120000, 5, 'https://swagshirts-4loawy6.stackpathdns.com/wp-content/uploads/2016/04/arc-reactor-full-sleeve-black-tee.jpg', '2018-07-07'),
('item03', 'Gareth Bale Official Mug', 30, 'Gareth Bale Official Mug ', 80000, 8, 'https://n4.sdlcdn.com/imgs/a/q/p/Artifa-Gareth-Bale-Football-Coffee-SDL960651117-2-be0a5.jpg', '2018-07-07'),
('item04', 'Dr Stephen Strange Human Body Anatomy', 300, 'Human Body Anatomy written by the one and only Dr Stephen Strange', 1200000, 3, 'https://cdn-rainbowresource.netdna-ssl.com/products/046938.jpg', '2018-07-07'),
('item05', 'Loris Karius\'s \'Power\' GK Glove', 100, 'Goalkeeping Glove by Lorus Karius.', 400000, 1, 'https://www.unitedcharity.de/var/charity_site/storage/images/auktionen/loris-karius-handschuhe/2760495-1-ger-DE/Loris-Karius-Handschuhe_reference.jpg', '2018-07-07');

-- --------------------------------------------------------

--
-- Table structure for table `kaloborators_transaction`
--

CREATE TABLE `kaloborators_transaction` (
  `kode_project` varchar(255) NOT NULL,
  `email_user` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kaloborators_transaction`
--

INSERT INTO `kaloborators_transaction` (`kode_project`, `email_user`) VALUES
('proj01', 'admin'),
('proj01', 'kevin@lspr.ac.id'),
('proj02', 'rona@yahoo.com'),
('proj03', 'admin'),
('proj03', 'tony@avengers.com'),
('proj04', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `kota`
--

CREATE TABLE `kota` (
  `nama_kota` varchar(255) NOT NULL,
  `nama_kecamatan` varchar(255) NOT NULL,
  `kode_pos` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kota`
--

INSERT INTO `kota` (`nama_kota`, `nama_kecamatan`, `kode_pos`) VALUES
('Jakarta Pusat', 'Gambir', '10110'),
('Jakarta Selatan', 'Pesanggrahan', '12320'),
('Jakarta Selatan', 'Pancoran', '12780'),
('Jakarta Selatan', 'Setiabudi', '12910'),
('Tangerang', 'Jatiuwung', '15134'),
('Tangerang', 'Cipete', '15142'),
('Tangerang', 'Cipondoh', '15148'),
('Bogor', 'Babakan', '16128'),
('Bogor', 'Lawanggintung', '16134'),
('Bogor', 'Kedungwaringin', '16163'),
('Bandung', 'Coblong', '40135'),
('Bandung', 'Gegerkalong', '40153'),
('Bandung', 'Binong', '40275');

-- --------------------------------------------------------

--
-- Table structure for table `kurir`
--

CREATE TABLE `kurir` (
  `nama_kurir` varchar(255) NOT NULL,
  `price_per_kg` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kurir`
--

INSERT INTO `kurir` (`nama_kurir`, `price_per_kg`) VALUES
('DHL', 10000),
('J&T Express ', 6000),
('JNE', 7000),
('Si Cepat', 8500),
('Tiki', 7000);

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `kode_project` varchar(255) NOT NULL,
  `email_user` varchar(255) NOT NULL,
  `nama_project` varchar(255) NOT NULL,
  `project_desc` varchar(255) NOT NULL,
  `target` varchar(255) NOT NULL,
  `no_kontak` varchar(255) NOT NULL,
  `status_project` varchar(255) NOT NULL,
  `required_person` int(11) NOT NULL,
  `due_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`kode_project`, `email_user`, `nama_project`, `project_desc`, `target`, `no_kontak`, `status_project`, `required_person`, `due_date`) VALUES
('proj01', 'lidya@student.mit.edu', 'Rachel Suprise Birthday ', 'Rachel Suprise Birthday ', 'Rachel', '021741258', 'EXPIRED', 5, '2018-07-06'),
('proj02', 'sonia@gmail.com', 'Ulang Tahun Papa', 'Suprise ulang tahun papa dengan memberi kado stik golf', 'Orantua Sonia', '08321654987', 'ONGOING', 3, '2018-07-08'),
('proj03', 'ilham@student.itb.ac.id', '5th Anniversary Suprise', 'Menghadiahkan pasangan dengan mengirim hadiah coklat dan sepatu.', 'Zara', '08987654321', 'DONE', 3, '2018-07-10'),
('proj04', 'kevin@lspr.ac.id', 'Melamar Pasangan ', 'Melakukan pelamaran pasangan di restoran', 'Ayaka', '08654123879', 'ONGOING', 9, '2018-07-09'),
('proj05', 'rona@yahoo.com', 'Sinka suprise birthday party', 'Star Wars themed birthday suprise part from rona to sinka', 'Sinka', '08741258963', 'ONGOING', 4, '2018-07-09');

-- --------------------------------------------------------

--
-- Table structure for table `transaction_item`
--

CREATE TABLE `transaction_item` (
  `kode_transaksi` varchar(255) NOT NULL,
  `email_user` varchar(255) NOT NULL,
  `kode_item` varchar(255) NOT NULL,
  `qty` int(11) NOT NULL,
  `kurir` varchar(255) NOT NULL,
  `total` int(11) NOT NULL,
  `nama_penerima` varchar(255) NOT NULL,
  `alamat_penerima` varchar(255) NOT NULL,
  `transaction_date` date NOT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaction_item`
--

INSERT INTO `transaction_item` (`kode_transaksi`, `email_user`, `kode_item`, `qty`, `kurir`, `total`, `nama_penerima`, `alamat_penerima`, `transaction_date`, `status`) VALUES
('trans01', 'admin', 'item01', 5, 'JNE', 332000, 'Arief', 'Jl. Dijkstra 15A , Jakarta Pusat Gambir , 10110.', '2018-07-03', 'WAITING FOR PAYMENT'),
('trans02', 'kevin@lspr.ac.id', 'item01', 1, 'Tiki', 72000, 'Adit', 'Jl. Moore 15A , Jakarta Selatan, Setiabudi , 12910.', '2018-07-03', 'SENT'),
('trans03', 'admin', 'item02', 1, 'Si Cepat', 128500, 'Edwin', 'Jl. Alan Turing 15A ,Jakarta Selatan , Pancoran , 12780.', '2018-06-11', 'SENT'),
('trans04', 'ilham@student.itb.ac.id', 'item04', 1, 'DHL', 1210000, 'Pratomo', 'Jl. Haber Bosch 15A ,Bandung ,Coblong  , 40135.', '2018-07-01', 'SENDING'),
('trans05', 'kevin@lspr.ac.id', 'item01', 10, 'J&T Express ', 656000, 'Bramantiyas', 'Jl. Pepaya , Jakarta Pusat Gambir , 10110.', '2018-06-26', 'SENT');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `email` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `hash` varchar(255) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `kode_pos` varchar(255) NOT NULL,
  `gift_point` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`email`, `nama`, `password`, `hash`, `tgl_lahir`, `kode_pos`, `gift_point`) VALUES
(' rona@yahoo.com', 'Rona Novita', '320d5ea519c7ebed2cfe11c6f74557ba', '123', '1997-05-06', '12320', 0),
('admin', 'admin', '0192023a7bbd73250516f069df18b500', '123', '1997-06-05', '15226', 5000),
('bagus@student.umn.ac.id', 'Bagus Indiarto', 'a89407b9014f6f6d9a85f2d5b6a2c118', '123', '1997-03-12', '15112', 0),
('gareth.bale@rmfc.es', 'Gareth Bale', '9094eda074a49c7a1b8e6996c7edf4cc', '123', '1989-07-16', '18515', 0),
('ilham@student.itb.ac.id', 'Ilham Julio', '57cf5ad49695e3adc1a29cf47a43bc06', '123', '1989-04-05', '12910', 0),
('kevin@lspr.ac.id', 'Kevin Feugie', 'd2e7a2105d0fb461fe6f2858cc33942f', '123', '0988-02-09', '12780', 0),
('lidya@student.mit.edu', 'Lidya Maulida', 'b855dd7d385f54ca7cdd72a454cfc90a', '123', '1996-08-17', '10110', 0),
('loris.karius@liverpool.com', 'Loris Karius', '2401ab3afd680b9c0999136f583aa313', '123', '1993-06-22', '92651', 0),
('sonia@gmail.com', 'Sonia Cornelia', 'fc827017dea930384d8768553c5c8caf', '123', '1998-08-09', '40135', 0),
('stephen.strange@avengers.com', 'Stephen Strange', '5615c86287c45c7e3a570df465376e3b', '123', '1978-08-08', '11324', 0),
('tony@avengers.com', 'Tony Stark', 'cc20f43c8c24dbc0b2539489b113277a', '123', '1980-01-17', '40135', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `item_sell`
--
ALTER TABLE `item_sell`
  ADD PRIMARY KEY (`kode_item`);

--
-- Indexes for table `kaloborators_transaction`
--
ALTER TABLE `kaloborators_transaction`
  ADD PRIMARY KEY (`kode_project`,`email_user`);

--
-- Indexes for table `kota`
--
ALTER TABLE `kota`
  ADD PRIMARY KEY (`kode_pos`);

--
-- Indexes for table `kurir`
--
ALTER TABLE `kurir`
  ADD PRIMARY KEY (`nama_kurir`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`kode_project`);

--
-- Indexes for table `transaction_item`
--
ALTER TABLE `transaction_item`
  ADD PRIMARY KEY (`kode_transaksi`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`email`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
